# Usar uma imagem base do Python 3.9 slim
FROM python:3.9-slim

# Definir o diretório de trabalho dentro do container
WORKDIR /app

# Copiar o arquivo de requisitos (dependências) para o diretório de trabalho
COPY requirements.txt .

# Instalar as dependências necessárias
RUN pip install --no-cache-dir -r requirements.txt

# Copiar todo o conteúdo da aplicação para o diretório de trabalho no container
COPY . .

# Expor a porta que a aplicação irá utilizar
EXPOSE 8000

# Comando para iniciar a aplicação (ajuste conforme necessário)
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
